package com.example.virtualrecipebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "Recipe.db";
    public static final String TABLE_NAME = "recipe_table";
    public static final String COL_1 = "id";
    public static final String COL_2 = "name";
    public static final String COL_3 = "image";
    public static final String COL_4 = "preparation_time";
    public static final String COL_5 = "cook_time";
    public static final String COL_6 = "description";
    public static final String TABLE_NAME_INGREDIENTS = "ingredients_table";
    public static final String COL2_INGREDIENTS = "name";
    public static final String COL3_RECIPE_ID_INGREDIENTS = "id_recipe";
    public static final String TABLE_NAME_STEPS = "steps_table";
    public static final String COL2_STEPS = "step";
    public static final String COL3_RECIPE_ID_STEPS = "id_recipe";
    public static final String TABLE_NAME_SHOPPING = "shopping_table";
    public static final String COL1_SHOPPING = "id";
    public static final String COL2_SHOPPING = "name";

    public DatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);


        SQLiteDatabase db = this.getWritableDatabase();
       // db.execSQL("delete from recipe_table");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " NAME TEXT, IMAGE BLOB, PREPARATION_TIME INTEGER, COOK_TIME INTEGER, DESCRIPTION TEXT)");
        db.execSQL("create table " + TABLE_NAME_INGREDIENTS + " (ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " NAME TEXT, ID_RECIPE INTEGER)");
        db.execSQL("create table " + TABLE_NAME_STEPS + " (ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                " STEP TEXT, ID_RECIPE INTEGER)");
        db.execSQL("create table " + TABLE_NAME_SHOPPING + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "NAME TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public void updateRecipe(Recipe recipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, recipe.getName());
        Bitmap b = recipe.getImage();
        byte[] img = BitmapEncoder.encode(b);
        contentValues.put(COL_3, img);
        contentValues.put(COL_4, recipe.getTimeForPreparation());
        contentValues.put(COL_5, recipe.getCookTime());
        contentValues.put(COL_6, recipe.getDescription());
        db.update(TABLE_NAME, contentValues, "id=?", new String[]{String.valueOf(recipe.getId())});
    }

    public void deleteRecipe(Recipe recipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME + " where id=?", new String[]{String.valueOf(recipe.getId())});

        deleteIngredients(recipe);
        deleteSteps(recipe);
    }

    public void deleteIngredients(Recipe recipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME_INGREDIENTS + " where id_recipe=?", new String[]{String.valueOf(recipe.getId())});
    }

    public void deleteSteps(Recipe recipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME_STEPS + " where id_recipe=?", new String[]{String.valueOf(recipe.getId())});
    }

    public boolean insertIngredient(String name, int idRecipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2_INGREDIENTS, name);
        contentValues.put(COL3_RECIPE_ID_INGREDIENTS, idRecipe);
        long result = db.insert(TABLE_NAME_INGREDIENTS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertStep(String step, int idRecipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2_STEPS, step);
        contentValues.put(COL3_RECIPE_ID_STEPS, idRecipe);
        long result = db.insert(TABLE_NAME_STEPS, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public boolean insertRecipe(Recipe recipe) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, recipe.getName());
        Bitmap b = recipe.getImage();
        byte[] img = BitmapEncoder.encode(b);
        contentValues.put(COL_3, img);
        contentValues.put(COL_4, recipe.getTimeForPreparation());
        contentValues.put(COL_5, recipe.getCookTime());
        contentValues.put(COL_6, recipe.getDescription());
        long result = db.insert(TABLE_NAME, null, contentValues);
        boolean ok = false;
        if (result == -1)
            ok = false;
        else
            ok = true;

        if (ok == false)
            return ok;

        Cursor res = db.rawQuery("SELECT last_insert_rowid();", null);
        int recipeId = 0;
        while (res.moveToNext())
            recipeId = res.getInt(0);

        for (String ingredient : recipe.getIngredients()) {
            insertIngredient(ingredient, recipeId);
        }
        recipe.getIngredients();

        for (String step : recipe.getSteps()) {
            insertStep(step, recipeId);
        }

        return ok;
    }

    public Cursor getAllRecipes() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);

        return res;
    }

    public Cursor getAllIngredientsForRecipe(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME_INGREDIENTS + " where " + COL3_RECIPE_ID_INGREDIENTS + "=?",
                new String[]{String.valueOf(id)});

        return res;
    }

    public Cursor getAllStepsForRecipe(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME_STEPS + " where " + COL3_RECIPE_ID_STEPS + "=?",
                new String[]{String.valueOf(id)});

        return res;
    }

    public Cursor getAllItemsToShop() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME_SHOPPING, null);

        return res;
    }

    public int getMaxIdFromShoppingTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select max(id) from " + TABLE_NAME_SHOPPING, null);

        res.moveToFirst();

        String maxIdStr = res.getString(0);
        int maxId = 0;
        if (maxIdStr != null)
            maxId = Integer.parseInt(maxIdStr);

        return maxId;
    }

    public boolean insertItemToBuy(String name) {
        int idItem = getMaxIdFromShoppingTable() + 1;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1_SHOPPING, idItem);
        contentValues.put(COL2_SHOPPING, name);
        long result = db.insert(TABLE_NAME_SHOPPING, null, contentValues);
        if (result == -1)
            return false;
        else
            return true;
    }

    public void updateItemToBuy(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, item.getName());
        db.update(TABLE_NAME_SHOPPING, contentValues, "id=?", new String[]{String.valueOf(item.getId())});
    }

    public void deleteItemToBuy(Item item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME_SHOPPING + " where id=?", new String[]{String.valueOf(item.getId())});
    }
}
