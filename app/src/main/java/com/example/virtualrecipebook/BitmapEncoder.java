package com.example.virtualrecipebook;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class BitmapEncoder {

    public static byte[] encode(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public static Bitmap decode(byte[] imgByte){
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds=false;
        return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length, opts);
    }
}
