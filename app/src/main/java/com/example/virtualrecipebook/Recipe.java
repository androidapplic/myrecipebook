package com.example.virtualrecipebook;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class Recipe {

    public void setId(Integer id) {
        this.id = id;
    }

    private Integer id;
    private String name;
    private Bitmap image;
    private int timeForPreparation;
    private int cookTime;
    private int totalTime;
    private String description;
    private List<String> ingredients;
    private List<String> steps;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public int getTimeForPreparation() {
        return timeForPreparation;
    }

    public void setTimeForPreparation(int timeForPreparation) {
        this.timeForPreparation = timeForPreparation;
    }

    public int getCookTime() {
        return cookTime;
    }

    public void setCookTime(int cookTime) {
        this.cookTime = cookTime;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(int totalTime) {
        this.totalTime = totalTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public List<String> getSteps() {
        return steps;
    }

    public void setSteps(List<String> steps) {
        this.steps = steps;
    }

    public Integer getId() { return id; }

    public Recipe(){
        ingredients = new ArrayList<>();
        steps = new ArrayList<>();
    }

    public Recipe(int id,
                  String name,
                  Bitmap imagePath,
                  int timeForPreparation,
                  int cookTime,
                  String description,
                  List<String> ingredients,
                  List<String> steps) {
        this.id = id;
        this.name = name;
        this.image = imagePath;
        this.timeForPreparation = timeForPreparation;
        this.cookTime = cookTime;
        this.description = description;
        this.ingredients = ingredients;
        this.steps = steps;
        this.totalTime = cookTime + timeForPreparation;
    }

    public String getStringIngredients() {
        StringBuilder ingredientsSb = new StringBuilder();

        for (int index = 0; index < ingredients.size() - 1; ++index) {
            ingredientsSb.append(ingredients.get(index) + "\n");
        }

        ingredientsSb.append(ingredients.get(ingredients.size() - 1));

        return ingredientsSb.toString();
    }

    public String getPreparationSteps(){
        StringBuilder preparationStepsSb = new StringBuilder();

        for(int index = 0;index<steps.size() - 1;++index){
            preparationStepsSb.append(steps.get(index)+ "\n");
        }

        preparationStepsSb.append(steps.get(steps.size() - 1));

        return preparationStepsSb.toString();
    }
}
