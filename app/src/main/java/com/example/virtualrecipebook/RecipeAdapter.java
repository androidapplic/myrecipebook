package com.example.virtualrecipebook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.ViewHolder> implements Filterable {

    private List<Recipe> recipes;
    private List<Recipe> recipesFull;

    public RecipeAdapter(Context context, ArrayList<Recipe> list) {
        recipes = list;
        recipesFull = new ArrayList<>(recipes);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;
        private ImageView ivMake;
        private TextView tvRecipeName;
        private TextView tvPreparationTime;
        private TextView tvCookTime;
        private TextView tvTotalTime;
        private TextView tvIngredients;
        private TextView tvRecipeDescription;
        private TextView tvPreparation;
        private FloatingActionButton deleteBtn;
        private FloatingActionButton editBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.cardView);
            ivMake = itemView.findViewById(R.id.ivMake);
            tvRecipeDescription = itemView.findViewById(R.id.tvRecipeDescription);
            tvRecipeName = itemView.findViewById(R.id.tvRecipeName);
            tvPreparationTime = itemView.findViewById(R.id.tvPreparationTime);
            tvCookTime = itemView.findViewById(R.id.tvCookTime);
            tvTotalTime = itemView.findViewById(R.id.tvTotalTime);
            tvIngredients = itemView.findViewById(R.id.tvIngredients);
            tvPreparation = itemView.findViewById(R.id.tvPreparation);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);
            editBtn = itemView.findViewById(R.id.editBtn);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

    @NonNull
    @Override
    public RecipeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecipeAdapter.ViewHolder holder, int position) {

        holder.itemView.setTag(recipes.get(position));
        holder.tvRecipeName.setText(recipes.get(position).getName());
        holder.tvRecipeDescription.setText(recipes.get(position).getDescription());
        holder.tvPreparationTime.setText("Preparation: " + String.valueOf(recipes.get(position).getTimeForPreparation()) + "min ");
        holder.tvCookTime.setText("Cook: " + String.valueOf(recipes.get(position).getCookTime()) + "min ");
        holder.tvTotalTime.setText("Total time: " + String.valueOf(recipes.get(position).getTotalTime()) + "min ");
        holder.tvIngredients.setText(recipes.get(position).getStringIngredients());
        holder.tvPreparation.setText(recipes.get(position).getPreparationSteps());
        holder.ivMake.setImageBitmap(recipes.get(position).getImage());

        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int indexRecipe = RecipeApplication.getRecipePositionByName(holder.tvRecipeName.getText().toString());
                RecipeApplication.databaseHelper.deleteRecipe(RecipeApplication.recipes.get(indexRecipe));
                RecipeApplication.generateRecipes();
                recipes = RecipeApplication.recipes;
                notifyDataSetChanged();
            }
        });

        holder.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddEditRecipeActivity.class);

                intent.putExtra("recipeName", holder.tvRecipeName.getText());
                intent.putExtra("recipeDescription", holder.tvRecipeDescription.getText());
                intent.putExtra("preparationTime", getDigits(holder.tvPreparationTime.getText()));
                intent.putExtra("cookTime", getDigits(holder.tvCookTime.getText()));
                intent.putExtra("ingredients", holder.tvIngredients.getText());
                intent.putExtra("preparationSteps", holder.tvPreparation.getText());

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                Bitmap bmp = ((BitmapDrawable)(holder.ivMake.getDrawable())).getBitmap();
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                intent.putExtra("recipeImageBytes", byteArray);
                v.getContext().startActivity(intent);
            }
        });
    }

    public static String getDigits(final CharSequence input){
        final StringBuilder sb = new StringBuilder(input.length());
        for(int index = 0; index < input.length(); index++){
            final char c = input.charAt(index);
            if(c > 47 && c < 58){
                sb.append(c);
            }
        }
        return sb.toString();
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }

    @Override
    public Filter getFilter() {
        return recipeFilter;
    }

    private Filter recipeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Recipe> filteredRecipes = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredRecipes.addAll(recipesFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Recipe recipe : recipesFull) {
                    if (recipe.getName().toLowerCase().contains(filterPattern)) {
                        filteredRecipes.add(recipe);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredRecipes;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            recipes.clear();
            recipes.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
}
