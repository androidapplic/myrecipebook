package com.example.virtualrecipebook;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddEditRecipeActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;

    private String initialNameOfRecipe;
    private TextView tvAddOrEdit;
    private TextView etName;
    private TextView etPreparationTime;
    private TextView etCookTime;
    private TextView etDescription;
    private FloatingActionButton addIngredientsBtn;
    private FloatingActionButton addPreparationStepsBtn;
    private LinearLayout ingredientsLl;
    private LinearLayout preparationStepsLl;
    private Button submitBtn;
    private Button takePhotoBtn;
    private ImageView ivRecipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);

        tvAddOrEdit = findViewById(R.id.tvAddOrEdit);
        etName = findViewById(R.id.etName);
        etPreparationTime = findViewById(R.id.etPreparationTime);
        etCookTime = findViewById(R.id.etCookTime);
        etDescription = findViewById(R.id.etDescription);
        addIngredientsBtn = findViewById(R.id.deleteItemBtn);
        addPreparationStepsBtn = findViewById(R.id.addPreparationStepsBtn);
        ingredientsLl = findViewById(R.id.ingredientsLl);
        preparationStepsLl = findViewById(R.id.preparationStepsLl);
        submitBtn = findViewById(R.id.submitBtn);
        takePhotoBtn = findViewById(R.id.takePhotoBtn);
        ivRecipe = findViewById(R.id.ivRecipe);

        addIngredientsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEditText(ingredientsLl, "Please, add an ingredient!");
            }
        });

        addPreparationStepsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEditText(preparationStepsLl, "Please, add a preparation step!");
            }
        });

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }
        });

        takePhotoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                } catch (ActivityNotFoundException e) {
                    // display error state to the user
                }
            }
        });

        setEditFields();
    }

    private void setEditFields(){
        Intent intent = getIntent();

        if (intent == null) {
            return;
        }

        if (!intent.hasExtra("recipeName")) {
            setTitle("Add Recipe");
            return;
        }

        setTitle("Edit Recipe");
        tvAddOrEdit.setText("Edit recipe");
        etName.setText(intent.getStringExtra("recipeName"));
        initialNameOfRecipe = etName.getText().toString();
        etPreparationTime.setText(intent.getStringExtra("preparationTime"));
        etCookTime.setText(intent.getStringExtra("cookTime"));
        etDescription.setText(intent.getStringExtra("recipeDescription"));
        addEditText(ingredientsLl, "Please, add an ingredient!", intent.getStringExtra("ingredients"));
        addEditText(preparationStepsLl, "Please, add a preparation step!", intent.getStringExtra("preparationSteps"));

        byte[] byteArray = intent.getByteArrayExtra("recipeImageBytes");
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        ivRecipe.setImageBitmap(bmp);
    }

    private List<String> getElementsFromLinearLayout(LinearLayout layout) {
        List<String> elements = new ArrayList<String>();
        ViewGroup group = (ViewGroup) layout;
        for (int index = 0; index < group.getChildCount(); ++index) {
            View view = group.getChildAt(index);
            if (view instanceof EditText) {
                String element = ((EditText) view).getText().toString();
                if (element.isEmpty() == false) {
                    elements.add(element);
                }
            }
        }
        return elements;
    }

    private void submit() {
        if (etName.getText().toString().isEmpty()) {
            Toast.makeText(AddEditRecipeActivity.this, "Please, fill name field!", Toast.LENGTH_SHORT).show();
        } else if (etPreparationTime.getText().toString().isEmpty()) {
            Toast.makeText(AddEditRecipeActivity.this, "Please, fill preparation time!", Toast.LENGTH_SHORT).show();
        } else if (etCookTime.getText().toString().isEmpty()) {
            Toast.makeText(AddEditRecipeActivity.this, "Please, fill cook time!", Toast.LENGTH_SHORT).show();
        } else if (etDescription.getText().toString().isEmpty()) {
            Toast.makeText(AddEditRecipeActivity.this, "Please, fill description field!", Toast.LENGTH_SHORT).show();
        } else {
            List<String> ingredients = getElementsFromLinearLayout(ingredientsLl);
            List<String> steps = getElementsFromLinearLayout(preparationStepsLl);

            if (ingredients.size() == 0) {
                Toast.makeText(AddEditRecipeActivity.this, "Please, fill ingredients fields!", Toast.LENGTH_SHORT).show();
            } else if (steps.size() == 0) {
                Toast.makeText(AddEditRecipeActivity.this, "Please, fill steps fields!", Toast.LENGTH_SHORT).show();
            } else {
                Recipe recipeToAdd = new Recipe(-1, etName.getText().toString(),
                        ((BitmapDrawable) ivRecipe.getDrawable()).getBitmap(),
                       // ((BitmapDrawable) getDrawable(R.drawable.cheesecake)).getBitmap(),
                        Integer.parseInt(etPreparationTime.getText().toString()),
                        Integer.parseInt(etCookTime.getText().toString()),
                        etDescription.getText().toString(),
                        ingredients,
                        steps);
                if (getTitle() == "Edit Recipe") {
                    int indexRecipe = RecipeApplication.getRecipePositionByName(initialNameOfRecipe);
                    if (indexRecipe != -1 && RecipeApplication.isNameAvailable(etName.getText().toString(), indexRecipe)) {
                        recipeToAdd.setId(RecipeApplication.recipes.get(indexRecipe).getId());
                        RecipeApplication.databaseHelper.updateRecipe(recipeToAdd);
                        Intent intent = new Intent(AddEditRecipeActivity.this, MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(AddEditRecipeActivity.this, "Your recipe was edited!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (RecipeApplication.isNameAvailable(etName.getText().toString())) {
                        RecipeApplication.databaseHelper.insertRecipe(recipeToAdd);

                        Intent intent = new Intent(AddEditRecipeActivity.this, MainActivity.class);
                        startActivity(intent);
                        Toast.makeText(AddEditRecipeActivity.this, "Your recipe was added!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddEditRecipeActivity.this, "The named of the recipe is already used. Please, change recipe name.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void addEditText(LinearLayout layout, String hintMessage, String text) {
        EditText et = new EditText(AddEditRecipeActivity.this);
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        et.setLayoutParams(p);
        et.setHint(hintMessage);
        et.setText(text);
        layout.addView(et);
    }

    private void addEditText(LinearLayout layout, String hintMessage) {
        EditText et = new EditText(AddEditRecipeActivity.this);
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        et.setLayoutParams(p);
        et.setHint(hintMessage);
        layout.addView(et);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            ivRecipe.setImageBitmap(imageBitmap);
        }
    }
}