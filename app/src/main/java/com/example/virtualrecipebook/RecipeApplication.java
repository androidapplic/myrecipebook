package com.example.virtualrecipebook;

import android.app.Application;
import android.database.Cursor;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class RecipeApplication extends Application {

    public static DatabaseHelper databaseHelper;
    public static ArrayList<Recipe> recipes;
    public static ArrayList<Item> items;

    @Override
    public void onCreate() {
        super.onCreate();
        databaseHelper = new DatabaseHelper(this);
        generateRecipes();
        items = new ArrayList<>();
        generateItems();
    }

    public static void generateRecipes() {
        recipes = new ArrayList<>();

        Cursor res = databaseHelper.getAllRecipes();
        if (res.getCount() == 0) {
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            Recipe recipe = new Recipe();
            recipe.setId(res.getInt(0));
            recipe.setName(res.getString(1));
            byte[] image = res.getBlob(2);
            recipe.setImage(BitmapEncoder.decode(image));
            recipe.setTimeForPreparation(Integer.parseInt(res.getString(3)));
            recipe.setCookTime(Integer.parseInt(res.getString(4)));
            recipe.setDescription(res.getString(5));
            recipes.add(recipe);
        }

        for (Recipe recipe : recipes) {
            res = databaseHelper.getAllIngredientsForRecipe(recipe.getId());
            while (res.moveToNext()) {
                recipe.getIngredients().add(res.getString(1));
            }

            res = databaseHelper.getAllStepsForRecipe(recipe.getId());
            while (res.moveToNext()) {
                recipe.getSteps().add(res.getString(1));
            }
        }

        for (Recipe recipe : recipes) {
            recipe.setTotalTime(recipe.getCookTime() + recipe.getTimeForPreparation());
        }
    }

    public static boolean isNameAvailable(String name) {
        for (Recipe recipe : recipes) {
            if (recipe.getName().trim().toLowerCase().equals(name.trim().toLowerCase()))
                return false;
        }
        return true;
    }

    public static boolean isNameAvailable(String name, int indexRecipe) {
        for (int index = 0; index < recipes.size(); ++index)
            if (index != indexRecipe && recipes.get(index).getName().trim().toLowerCase().equals(name.trim().toLowerCase()))
                return false;
        return true;
    }

    public static int getRecipePositionByName(String name) {
        for (int index = 0; index < recipes.size(); ++index)
            if (recipes.get(index).getName().trim().toLowerCase().equals(name.trim().toLowerCase()))
                return index;
        return -1;
    }

    public static void generateItems() {
        items.clear();
        Cursor res = databaseHelper.getAllItemsToShop();
        if (res.getCount() == 0) {
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            Item item = new Item();
            item.setId(res.getInt(0));
            item.setName(res.getString(1));

            items.add(item);
        }
    }
}
