package com.example.virtualrecipebook;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ShoppingActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter myAdapter;
    RecyclerView.LayoutManager layoutManager;

    Button addItemBtn;
    TextView etItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);

        recyclerView = findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        myAdapter = new ShoppingItemsAdapter(this, RecipeApplication.items);
        recyclerView.setAdapter(myAdapter);

        addItemBtn = findViewById(R.id.addItemBtn);
        etItem = findViewById(R.id.etItem);

        addItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etItem.getText().toString().trim().isEmpty()) {
                    Toast.makeText(ShoppingActivity.this, "Please, fill item to buy!", Toast.LENGTH_SHORT).show();
                    return;
                }

                RecipeApplication.databaseHelper.insertItemToBuy(etItem.getText().toString());
                RecipeApplication.generateItems();
                myAdapter.notifyDataSetChanged();
                etItem.setText("");
            }
        });
    }
}